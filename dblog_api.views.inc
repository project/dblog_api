<?php

/**
 * @file
 * Views API hooks for the dblog_api module.
 */

/**
 * Implements hook_views_data_alter().
 *
 * Replace the watchdog link field handler plugin with our own.
 */
function dblog_api_views_data_alter(array &$data) {
  $data['watchdog']['link']['field']['id'] = 'dblog_api_operations';
}
