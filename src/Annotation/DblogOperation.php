<?php

namespace Drupal\dblog_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * An annotation to mark a DblogOperation plugin.
 *
 * @Annotation
 */
class DblogOperation extends Plugin {

  /**
   * The DblogOperation plugin ID.
   *
   * @var string
   */
  public $id;

}
