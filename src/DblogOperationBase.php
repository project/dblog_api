<?php

namespace Drupal\dblog_api;

use Drupal\Core\Plugin\PluginBase;

/**
 * A base for DblogOperation plugins.
 */
abstract class DblogOperationBase extends PluginBase {}
