<?php

namespace Drupal\dblog_api;

use Drupal\views\ResultRow;

/**
 * An interface for DblogOperation plugins.
 */
interface DblogOperationInterface {

  /**
   * Display an operation in the operations column of a dblog message.
   *
   * @param \Drupal\views\ResultRow $dblogRow
   *   The data in the dblog row that is being displayed.
   *
   * @return array
   *   A render array.
   */
  public function displayOperation(ResultRow $dblogRow) : array;

  /**
   * State whether this plugin should try to display an operation.
   *
   * @param \Drupal\views\ResultRow $dblogRow
   *   The data in the dblog row that is being displayed.
   *
   * @return bool
   *   TRUE if this function should try to display an operation; FALSE
   *   otherwise.
   */
  public function shouldDisplay(ResultRow $dblogRow) : bool;

}
