<?php

namespace Drupal\dblog_api;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * A plugin manager for adding entries to the dblog operations column.
 */
class DblogOperationManager extends DefaultPluginManager {

  /**
   * Constructs a DblogOperationManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/DblogOperation', $namespaces, $module_handler,
      'Drupal\dblog_api\DblogOperationInterface',
      'Drupal\dblog_api\Annotation\DblogOperation');

    $this->alterInfo('dblog_api_operation');
    $this->setCacheBackend($cache_backend, 'dblog_api_operation');
  }

}
