<?php

namespace Drupal\dblog_api\Plugin\views\field;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dblog\Plugin\views\field\DblogOperations;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A field handler that renders dblog operations column markup.
 *
 * @ViewsField("dblog_api_operations")
 */
class DblogApiOperations extends DblogOperations implements ContainerFactoryPluginInterface {

  /**
   * A plugin manager for adding entries to the dblog operations column.
   *
   * @var \Drupal\dblog_api\DblogOperationManager
   */
  protected $dblogOperationsManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->dblogOperationsManager = $container->get('plugin.manager.dblog_api_operation');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // Render dblog's "view" link into its own part of the render array.
    $output['dblog_view']['#markup'] = parent::render($values);

    // Loop through each DbLog Operations plugins, ask each if they want to
    // contribute, and if so, ask them to display.
    foreach ($this->dblogOperationsManager->getDefinitions() as $pluginDef) {
      $pluginId = $pluginDef['id'];
      /** @var \Drupal\dblog_api\DblogOperationInterface $plugin */
      $plugin = $this->dblogOperationsManager->createInstance($pluginId);
      if ($plugin->shouldDisplay($values)) {
        $output[$pluginId] = $plugin->displayOperation($values);
      }
    }

    return $output;
  }

}
