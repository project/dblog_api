<?php

namespace Drupal\dblog_api_test\Plugin\DblogOperation;

use Drupal\dblog_api\DblogOperationBase;
use Drupal\dblog_api\DblogOperationInterface;
use Drupal\views\ResultRow;

/**
 * A test dblog operation that outputs "dblog_api_test_test_1".
 *
 * @DblogOperation(
 *   id = "dblog_api_test_test_1",
 * )
 */
class TestDblogOperationOne extends DblogOperationBase implements DblogOperationInterface {

  /**
   * {@inheritdoc}
   */
  public function displayOperation(ResultRow $dblogRow) : array {
    return ['#markup' => ' ' . $this->t('dblog_api_test_test_1') . ' '];
  }

  /**
   * {@inheritdoc}
   */
  public function shouldDisplay(ResultRow $dblogRow) : bool {
    return TRUE;
  }

}
