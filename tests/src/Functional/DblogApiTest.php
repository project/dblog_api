<?php

namespace Drupal\Tests\dblog_api\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\Component\Utility\Unicode;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\dblog\Functional\FakeLogEntries;

/**
 * Tests the dblog_api module.
 *
 * @group dblog_api
 */
class DblogApiTest extends BrowserTestBase {
  use FakeLogEntries;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dblog',
    'dblog_api',
    'dblog_api_test',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that the dblog_api module allows inserting arbitrary operations.
   */
  public function testDblogApi() {
    // Create a node and give it a log entry.
    $node = $this->drupalCreateNode();
    $link = $node->toLink('View', 'canonical')->toString();
    $this->generateLogEntries(1, [
      'message' => 'Log entry with link',
      'link' => $link,
    ]);

    // Generate a log entry without a link.
    $this->generateLogEntries(1, [
      'message' => 'Log entry without link',
    ]);

    // Log in as a user with permission to see the dblog, and visit the dblog.
    $this->drupalLogin($this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
      'access site reports',
    ]));
    $this->drupalGet('/admin/config/development/performance');
    $this->submitForm([], 'Clear all caches');

    $this->drupalGet('admin/reports/dblog');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Recent log messages');
    $this->assertLogMessage('dblog_api_test module installed.', 'Log indicates dblog_api_test module installed.');

    // Check the log message with a link and its operations column.
    $this->assertLogMessage('Log entry with link', 'Log entry with link found.');
    $this->assertLogMessageOperationsColumnContains('Log entry with link', $link, 'Operations column for log with link contains expected URL added by dblog core.');
    $this->assertLogMessageOperationsColumnContains('Log entry with link', 'dblog_api_test_test_1', 'Operations column for log with link contains test module text.');
    $this->assertLogMessageOperationsColumnContains('Log entry with link', 'dblog_api_test_test_2', 'Operations column for log with link contains test module text.');

    // Check the log message without a link and its operations column.
    $this->assertLogMessage('Log entry without link', 'Log entry without link found.');
    $this->assertLogMessageOperationsColumnContains('Log entry without link', 'dblog_api_test_test_1', 'Operations column for log without link contains test module text.');
    $this->assertLogMessageOperationsColumnContains('Log entry without link', 'dblog_api_test_test_2', 'Operations column for log without link contains test module text.');
  }

  /**
   * Confirms that a log message appears on the database log overview screen.
   *
   * This function should only be used for the admin/reports/dblog page, because
   * it checks for the message link text truncated to 56 characters. Other log
   * pages have no detail links so they contain the full message text.
   *
   * @param string $log_message
   *   The database log message to check.
   * @param string $message
   *   The message to pass to simpletest.
   */
  protected function assertLogMessage(string $log_message, string $message): void {
    $message_text = Unicode::truncate($log_message, 56, TRUE, TRUE);
    $this->assertSession()->linkExists($message_text, 0, $message);
  }

  /**
   * Confirms that the operations column contains text in a row containing text.
   *
   * @param string $logMessage
   *   The database log message to check.
   * @param string $textInOpsColumn
   *   The operations column text to check.
   * @param string $message
   *   The message to pass to simpletest.
   */
  protected function assertLogMessageOperationsColumnContains(string $logMessage, string $textInOpsColumn, string $message): void {
    // Account for default message trim length.
    $truncLogMessage = Unicode::truncate($logMessage, 56, TRUE, TRUE);

    // Look in the document for an element whose text contains $logMessage. Then
    // go up to the nearest-tr-ancestor, and search inside it for the last td
    // element, i.e.: the operations column.
    $matchingOpsColumns = $this->xpath('.//*[contains(text(), :logMessage)]/ancestor::tr/td[last()]', [
      ':logMessage' => $truncLogMessage,
    ]);
    $opsColumn = reset($matchingOpsColumns);

    $this->assertInstanceOf(NodeElement::class, $opsColumn, sprintf('Could not find row matching text %s', $truncLogMessage));
    $this->assertStringContainsString($textInOpsColumn, $opsColumn->getHtml(), $message);
  }

}
